using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public Material red;
    public Material gray;
    public Material green;
    bool isClicked = false;
 
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CastRay(red);
        }
        if (Input.GetMouseButtonDown(1))
        {
            CastRay(green);
        }
    }

    void CastRay(Material color)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
        if (hit.collider != null && hit.collider.gameObject.name == gameObject.name)
        {
            GetComponent<SpriteRenderer>().material = isClicked ? gray : color;
            isClicked = !isClicked;
        }
    }
}
