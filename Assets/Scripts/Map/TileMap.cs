﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class TileMap : MonoBehaviour
{
    public GameObject background, tile;


    void Start()
    {
        GenerateMapVisual();
    }

    (float width, float height) GetSize(GameObject gameObject)
    {
        var renderer = gameObject.GetComponent<Renderer>();

        return (renderer.bounds.size.x, renderer.bounds.size.y);
    }

    (int mapX, int mapY) GetMapSize(GameObject background, GameObject tile)
    {
        var (backgroundSizeX, backgroundSizeY) = GetSize(background);
        var (tileSizeX, tileSizeY) = GetSize(tile);
        int mapSizeX = (int)(backgroundSizeX / tileSizeX) + 1;
        int mapSizeY = (int)(backgroundSizeY / tileSizeY) + 1;

        return (mapSizeX, mapSizeY);
    }

    (float posX, float posY) GetPosition(GameObject background, GameObject tile)
    {
        var (backgroundSizeX, backgroundSizeY) = GetSize(background);
        var (tileSizeX, tileSizeY) = GetSize(tile);

        float posX = -backgroundSizeX / 2 + (tileSizeX / 2);
        float posY = -backgroundSizeY / 2 + (tileSizeY / 2);
        return (posX, posY);
    }

    void GenerateMapVisual()
    {
        var (tileSizeX, tileSizeY) = GetSize(tile);
        var (mapSizeX, mapSizeY) = GetMapSize(background, tile);
        var (posX, posY) = GetPosition(background, tile);

        float posYZero = posY;


        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                GameObject go = Instantiate(tile, new Vector3(posX, posY, 1), Quaternion.identity);
                go.name = "tile_(" + x + " " + y + ")";

                posY += tileSizeY;
            }
            posX += tileSizeX;
            posY = posYZero;

        }
    }

}
